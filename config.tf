terraform {
  backend "s3" {
    bucket = "cmcc-terraform-state"
    key = "mighty_trousers/terraform.tfstate"
    region = "eu-central-1"
    dynamodb_table = "cmcc_terraform_lock"
  }
}
